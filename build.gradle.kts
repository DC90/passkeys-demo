plugins {
    id("com.android.application") apply false
    id("com.android.library") apply false
    id("com.github.johnrengelman.shadow") apply false
    id("com.google.dagger.hilt.android") apply false
    id("io.ktor.plugin") apply false
    kotlin("android") apply false
    kotlin("js") apply false
    kotlin("jvm") apply false
    kotlin("multiplatform") apply false
    kotlin("plugin.serialization") apply false
}

buildscript {
    dependencies {
        classpath("com.google.gms:google-services:4.3.15")
    }
}

group = "dev.dc.passkeys.demo"
version = "1.0-SNAPSHOT"
