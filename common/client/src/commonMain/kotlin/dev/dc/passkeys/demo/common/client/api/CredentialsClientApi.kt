package dev.dc.passkeys.demo.common.client.api

import dev.dc.passkeys.demo.common.client.engine
import dev.dc.passkeys.demo.common.model.api.CredentialsApi
import dev.dc.passkeys.demo.common.model.dto.PostCredentialCreationOptionsRequest
import dev.dc.passkeys.demo.common.model.dto.PostCredentialCreationOptionsResponse
import dev.dc.passkeys.demo.common.model.dto.PostCredentialRequestOptionsRequest
import dev.dc.passkeys.demo.common.model.dto.PostCredentialRequestOptionsResponse
import dev.dc.passkeys.demo.common.model.entity.PublicKeyCredential
import dev.dc.passkeys.demo.common.model.resource.Api.*
import io.ktor.client.*
import io.ktor.client.call.*
import io.ktor.client.plugins.*
import io.ktor.client.plugins.contentnegotiation.*
import io.ktor.client.plugins.resources.*
import io.ktor.client.request.*
import io.ktor.http.*
import io.ktor.serialization.kotlinx.json.*

class CredentialsClientApi(
    hostUrl: String,
    private val client: HttpClient = HttpClient(engine) {
        install(ContentNegotiation) {
            json()
        }
        install(Resources)
        defaultRequest {
            url(hostUrl)
        }
    }
) : CredentialsApi {
    override suspend fun getCredentials(): Collection<PublicKeyCredential> {
        return client.get(Credentials()).body()
    }

    override suspend fun getCredential(id: String): PublicKeyCredential {
        return client.get(Credentials.ById(id = id)).body()
    }

    override suspend fun postCredential(credential: PublicKeyCredential): PublicKeyCredential {
        return client.post(Credentials()) {
            contentType(ContentType.Application.Json)
            setBody(credential)
        }.body()
    }

    override suspend fun postCredentialCreationOptions(request: PostCredentialCreationOptionsRequest): PostCredentialCreationOptionsResponse {
        return client.post(CredentialCreationOptions()) {
            contentType(ContentType.Application.Json)
            setBody(request)
        }.body()
    }

    override suspend fun postCredentialRequestOptions(request: PostCredentialRequestOptionsRequest): PostCredentialRequestOptionsResponse {
        return client.post(CredentialRequestOptions()) {
            contentType(ContentType.Application.Json)
            setBody(request)
        }.body()
    }
}
