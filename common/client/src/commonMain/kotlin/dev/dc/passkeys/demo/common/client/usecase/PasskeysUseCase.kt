package dev.dc.passkeys.demo.common.client.usecase

import dev.dc.passkeys.demo.common.model.api.CredentialsApi
import dev.dc.passkeys.demo.common.model.dto.PostCredentialCreationOptionsRequest
import dev.dc.passkeys.demo.common.model.dto.PostCredentialRequestOptionsRequest
import dev.dc.passkeys.demo.common.model.entity.CredentialCreationOptions
import dev.dc.passkeys.demo.common.model.entity.CredentialRequestOptions
import dev.dc.passkeys.demo.common.model.entity.PublicKeyCredential

interface CredentialsInteractor {
    suspend fun createCredential(options: CredentialCreationOptions): PublicKeyCredential
    suspend fun getCredential(options: CredentialRequestOptions): PublicKeyCredential
}

class PasskeysUseCase(
    private val api: CredentialsApi,
    private val interactor: CredentialsInteractor
) {
    suspend fun createPasskey(name: String, email: String) = runCatching {
        val options = api.postCredentialCreationOptions(
            request = PostCredentialCreationOptionsRequest(
                displayName = name,
                username = email
            )
        )

        val credential = interactor.createCredential(options)

        api.postCredential(credential)
    }

    suspend fun getPasskey() = runCatching {
        val options = api.postCredentialRequestOptions(
            request = PostCredentialRequestOptionsRequest(
                username = "username"
            )
        )

        val credential = interactor.getCredential(options)

        api.postCredential(credential)
    }

    suspend fun getCredentialById(id: String) = runCatching {
        api.getCredential(id)
    }
}
