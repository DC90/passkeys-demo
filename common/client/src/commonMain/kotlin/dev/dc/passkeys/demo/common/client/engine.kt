package dev.dc.passkeys.demo.common.client

import io.ktor.client.engine.*

expect val engine: HttpClientEngine
