package dev.dc.passkeys.demo.common.client

import io.ktor.client.engine.okhttp.*

actual val engine = OkHttp.create()
