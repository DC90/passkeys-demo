package dev.dc.passkeys.demo.common.client.usecase

import android.content.Context
import androidx.credentials.*
import dev.dc.passkeys.demo.common.model.entity.CredentialCreationOptions
import dev.dc.passkeys.demo.common.model.entity.CredentialRequestOptions
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import dev.dc.passkeys.demo.common.model.entity.PublicKeyCredential as PublicKeyCredentialEntity

class AndroidCredentialsInteractor(
    private val context: Context,
    private val credentialsManager: CredentialManager = CredentialManager.create(context)
) : CredentialsInteractor {
    override suspend fun createCredential(options: CredentialCreationOptions): PublicKeyCredentialEntity {
        val request = CreatePublicKeyCredentialRequest(
            requestJson = Json.encodeToString(options.publicKey)
        )

        val credential = credentialsManager.createCredential(
            context = context,
            request = request
        ) as CreatePublicKeyCredentialResponse

        return Json.decodeFromString(credential.registrationResponseJson)
    }

    override suspend fun getCredential(options: CredentialRequestOptions): PublicKeyCredentialEntity {
        val request = GetCredentialRequest(
            credentialOptions = listOf(
                GetPublicKeyCredentialOption(
                    requestJson = Json.encodeToString(options.publicKey)
                )
            )
        )

        val credential = credentialsManager.getCredential(
            context = context,
            request = request
        ).credential as PublicKeyCredential

        return Json.decodeFromString(credential.authenticationResponseJson)
    }
}
