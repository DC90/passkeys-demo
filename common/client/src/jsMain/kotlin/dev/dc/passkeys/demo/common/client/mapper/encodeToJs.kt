package dev.dc.passkeys.demo.common.client.mapper

import dev.dc.passkeys.demo.common.model.entity.*
import js.core.jso
import js.typedarrays.Uint8Array
import web.authn.AttestationConveyancePreference
import web.authn.AuthenticatorAttachment
import web.authn.AuthenticatorTransport
import web.authn.PublicKeyCredentialType
import web.authn.ResidentKeyRequirement
import web.authn.UserVerificationRequirement
import web.credentials.CredentialMediationRequirement

fun CredentialCreationOptions.encodeToJs(): web.credentials.CredentialCreationOptions = let {
    jso {
        publicKey = it.publicKey.encodeToJs()
    }
}

fun CredentialRequestOptions.encodeToJs(): web.credentials.CredentialRequestOptions = let {
    jso {
        publicKey = it.publicKey.encodeToJs()
        mediation = CredentialMediationRequirement.valueOf(it.mediation)
    }
}

fun PublicKeyCredentialCreationOptions.encodeToJs(): web.authn.PublicKeyCredentialCreationOptions = let {
    jso {
        attestation = AttestationConveyancePreference.valueOf(it.attestation)
        authenticatorSelection = it.authenticatorSelection?.encodeToJs()
        challenge = it.challenge.encodeToUint8Array()
        excludeCredentials = it.excludeCredentials.map(PublicKeyCredentialDescriptor::encodeToJs).toTypedArray()
        extensions = it.extensions?.let {
            jso {
                appid = it["appid"] as? String
                credProps = it["credProps"] as? Boolean
                hmacCreateSecret = it["hmacCreateSecret"] as? Boolean
            }
        }
        pubKeyCredParams = it.pubKeyCredParams.map(PublicKeyCredentialParameters::encodeToJs).toTypedArray()
        rp = it.rp.encodeToJs()
        timeout = it.timeout
        user = it.user.encodeToJs()
    }
}

fun PublicKeyCredentialRequestOptions.encodeToJs(): web.authn.PublicKeyCredentialRequestOptions = let {
    jso {
        allowCredentials = it.allowCredentials.map(PublicKeyCredentialDescriptor::encodeToJs).toTypedArray()
        challenge = it.challenge.encodeToUint8Array()
        extensions = it.extensions?.let {
            jso {
                appid = it["appid"] as? String
                credProps = it["credProps"] as? Boolean
                hmacCreateSecret = it["hmacCreateSecret"] as? Boolean
            }
        }
        rpId = it.rpId
        timeout = it.timeout
        userVerification = UserVerificationRequirement.valueOf(it.userVerification)
    }
}

fun AuthenticatorSelectionCriteria.encodeToJs(): web.authn.AuthenticatorSelectionCriteria = let {
    jso {
        authenticatorAttachment = AuthenticatorAttachment.valueOf(it.authenticatorAttachment)
        requireResidentKey = it.requireResidentKey
        residentKey = ResidentKeyRequirement.valueOf(it.residentKey)
    }
}

fun PublicKeyCredentialDescriptor.encodeToJs(): web.authn.PublicKeyCredentialDescriptor = let {
    jso {
        id = it.id.encodeToUint8Array()
        type = PublicKeyCredentialType.valueOf(it.type)
        transports = it.transports.mapNotNull(AuthenticatorTransport::valueOf).toTypedArray()
    }
}

fun PublicKeyCredentialParameters.encodeToJs(): web.authn.PublicKeyCredentialParameters = let {
    jso {
        alg = it.alg
        type = PublicKeyCredentialType.valueOf(it.type)
    }
}

fun PublicKeyCredentialRpEntity.encodeToJs(): web.authn.PublicKeyCredentialRpEntity = let {
    jso {
        id = it.id
        name = it.name
    }
}

fun PublicKeyCredentialUserEntity.encodeToJs(): web.authn.PublicKeyCredentialUserEntity = let {
    jso {
        id = it.id.encodeToUint8Array()
        name = it.name
        displayName = it.displayName.orEmpty()
    }
}

private fun String.encodeToUint8Array() = Uint8Array(encodeToByteArray().toTypedArray())
