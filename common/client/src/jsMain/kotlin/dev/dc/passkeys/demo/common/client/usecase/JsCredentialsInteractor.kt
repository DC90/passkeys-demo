package dev.dc.passkeys.demo.common.client.usecase

import dev.dc.passkeys.demo.common.client.mapper.decodeFromJs
import dev.dc.passkeys.demo.common.client.mapper.encodeToJs
import dev.dc.passkeys.demo.common.model.entity.CredentialCreationOptions
import dev.dc.passkeys.demo.common.model.entity.CredentialRequestOptions
import js.core.jso
import kotlinx.coroutines.await
import web.authn.PublicKeyCredential
import web.credentials.CredentialsContainer
import web.navigator.navigator
import dev.dc.passkeys.demo.common.model.entity.PublicKeyCredential as PublicKeyCredentialEntity
import web.credentials.CredentialCreationOptions as CredentialCreationOptionsRequest
import web.credentials.CredentialRequestOptions as CredentialRequestOptionsRequest

class JsCredentialsInteractor(
    private val credentials: CredentialsContainer = navigator.credentials
) : CredentialsInteractor {
    override suspend fun createCredential(options: CredentialCreationOptions): PublicKeyCredentialEntity {
        val request = jso<CredentialCreationOptionsRequest> {
            publicKey = options.publicKey.encodeToJs()
        }

        val credential = credentials.create(
            options = request
        ).await() as PublicKeyCredential

        return credential.decodeFromJs()
    }

    override suspend fun getCredential(options: CredentialRequestOptions): PublicKeyCredentialEntity {
        val request = jso<CredentialRequestOptionsRequest> {
            publicKey = options.publicKey.encodeToJs()
        }

        val credential = credentials.get(
            options = request
        ).await() as PublicKeyCredential

        return credential.decodeFromJs()
    }
}
