package dev.dc.passkeys.demo.common.client.mapper

import web.authn.*
import web.credentials.CredentialMediationRequirement

fun AttestationConveyancePreference.Companion.valueOf(rawValue: String?) = when (rawValue) {
    "direct" -> direct
    "enterprise" -> enterprise
    "indirect" -> indirect
    "none" -> none
    else -> null
}

fun AuthenticatorTransport.Companion.valueOf(rawValue: String?) = when (rawValue) {
    "ble" -> ble
    "hybrid" -> hybrid
    "internal" -> internal
    "nfc" -> nfc
    "usb" -> usb
    else -> null
}

fun AuthenticatorAttachment.Companion.valueOf(rawValue: String?) = when (rawValue) {
    "crossPlatform" -> crossPlatform
    "platform" -> platform
    else -> null
}

fun CredentialMediationRequirement.Companion.valueOf(rawValue: String?) = when (rawValue) {
    "conditional" -> conditional
    "optional" -> optional
    "required" -> required
    "silent" -> silent
    else -> null
}

fun PublicKeyCredentialType.Companion.valueOf(rawValue: String?) = when (rawValue) {
    "publicKey" -> publicKey
    else -> publicKey
}

fun ResidentKeyRequirement.Companion.valueOf(rawValue: String?) = when (rawValue) {
    "discouraged" -> discouraged
    "preferred" -> preferred
    "required" -> required
    else -> null
}

fun UserVerificationRequirement.Companion.valueOf(rawValue: String?) = when (rawValue) {
    "discouraged" -> discouraged
    "preferred" -> preferred
    "required" -> required
    else -> null
}
