package dev.dc.passkeys.demo.common.client.mapper

import dev.dc.passkeys.demo.common.model.entity.AuthenticatorAssertionResponse
import dev.dc.passkeys.demo.common.model.entity.AuthenticatorAttestationResponse
import dev.dc.passkeys.demo.common.model.entity.PublicKeyCredential
import js.buffer.ArrayBuffer
import js.typedarrays.Uint8Array
import web.encoding.TextDecoder
import kotlin.io.encoding.Base64
import kotlin.io.encoding.ExperimentalEncodingApi

fun web.authn.PublicKeyCredential.decodeFromJs() = PublicKeyCredential(
    id = id,
    rawId = rawId.base64UrlEncode(),
    response = response.decodeFromJs(),
    type = type
)

fun web.authn.AuthenticatorResponse.decodeFromJs() = when (this) {
    is web.authn.AuthenticatorAssertionResponse -> decodeFromJs()
    is web.authn.AuthenticatorAttestationResponse -> decodeFromJs()
    else -> throw IllegalArgumentException("Invalid type: ${this::class}")
}

fun web.authn.AuthenticatorAttestationResponse.decodeFromJs() = AuthenticatorAttestationResponse(
    clientDataJSON = clientDataJSON.decodeToString(),
    attestationObject = getAuthenticatorData().base64UrlEncode(),
    transports = getTransports().toList()
)

fun web.authn.AuthenticatorAssertionResponse.decodeFromJs() = AuthenticatorAssertionResponse(
    clientDataJSON = clientDataJSON.decodeToString(),
    authenticatorData = authenticatorData.base64UrlEncode(),
    signature = signature.base64UrlEncode(),
    userHandle = userHandle?.decodeToString()
)

@OptIn(ExperimentalEncodingApi::class)
private fun ArrayBuffer.base64UrlEncode() = Base64.UrlSafe.encode(Uint8Array(this).unsafeCast<ByteArray>())

private fun ArrayBuffer.decodeToString() = TextDecoder().decode(this)