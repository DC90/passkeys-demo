plugins {
    id("com.android.library")
    kotlin("multiplatform")
    kotlin("plugin.serialization")
}

group = "${rootProject.group}.common.client"
version = rootProject.version

android {
    namespace = group.toString()
    compileSdk = 33

    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_17
        targetCompatibility = JavaVersion.VERSION_17
    }
}

kotlin {
    android()
    js(IR) {
        binaries.executable()
        browser()
    }

    targets {
        println(this.asMap)
    }

    sourceSets {
        named("androidMain") {
            dependencies {
                implementation(libs.ktor.client.okhttp)
                implementation(libs.bundles.androidx.credentials)
            }
        }
        named("commonMain") {
            dependencies {
                implementation(project(":common:model"))
                implementation(libs.kotlinx.serialization.json)
                implementation(libs.ktor.client.contentnegotiation)
                implementation(libs.ktor.client.core)
                implementation(libs.ktor.client.resources)
                implementation(libs.ktor.serialization.kotlinx.json)
            }
        }
        named("jsMain") {
            dependencies {
                implementation(libs.ktor.client.js)
                implementation(enforcedPlatform("org.jetbrains.kotlin-wrappers:kotlin-wrappers-bom:1.0.0-pre.567"))
                implementation("org.jetbrains.kotlin-wrappers:kotlin-browser")
            }
        }
    }
}
