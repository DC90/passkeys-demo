plugins {
    id("com.android.library")
    kotlin("multiplatform")
    kotlin("plugin.serialization")
}

group = "${rootProject.group}.common.model"
version = rootProject.version

android {
    namespace = group.toString()
    compileSdk = 33

    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_17
        targetCompatibility = JavaVersion.VERSION_17
    }
}

kotlin {
    android()
    js(IR) {
        binaries.executable()
        browser()
    }
    jvm {
        jvmToolchain(17)
    }

    sourceSets {
        named("commonMain") {
            dependencies {
                implementation(libs.kotlinx.serialization.json)
                implementation(libs.ktor.resources)
            }
        }
    }
}
