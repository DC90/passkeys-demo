package dev.dc.passkeys.demo.common.model.entity

@Target(AnnotationTarget.PROPERTY, AnnotationTarget.TYPE)
@Retention(AnnotationRetention.SOURCE)
annotation class COSEAlgorithmIdentifier {
    companion object {
        @COSEAlgorithmIdentifier
        const val RS256 = -257

        @COSEAlgorithmIdentifier
        const val ES256 = -7
    }
}
