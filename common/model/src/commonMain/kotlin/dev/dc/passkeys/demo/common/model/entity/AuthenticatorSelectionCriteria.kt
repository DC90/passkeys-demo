package dev.dc.passkeys.demo.common.model.entity

import kotlinx.serialization.Serializable

/**
 *  Specify requirements regarding authenticator attributes.
 *
 *  @property authenticatorAttachment If present, filter eligible authenticators given the specified [AuthenticatorAttachment] modality
 *  @property residentKey The extent to which the Relying Party desires a client-side discoverable credential
 *  @property requireResidentKey retained for backwards compatibility, set true if [residentKey] is [ResidentKeyRequirement.REQUIRED]
 *  @property userVerification The Relying Party's requirement for user verification when creating the credential
 */
@Serializable
data class AuthenticatorSelectionCriteria(
    val authenticatorAttachment: @AuthenticatorAttachment String? = null,
    val residentKey: @ResidentKeyRequirement String? = null,
    val requireResidentKey: Boolean = false,
    val userVerification: @UserVerificationRequirement String? = UserVerificationRequirement.PREFERRED
)
