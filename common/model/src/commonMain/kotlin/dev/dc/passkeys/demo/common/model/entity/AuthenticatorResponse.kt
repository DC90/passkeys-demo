package dev.dc.passkeys.demo.common.model.entity

import kotlinx.serialization.Serializable
import kotlinx.serialization.json.JsonContentPolymorphicSerializer
import kotlinx.serialization.json.JsonElement
import kotlinx.serialization.json.jsonObject

@Serializable(with = AuthenticatorResponseSerializer::class)
sealed interface AuthenticatorResponse {
    val clientDataJSON: String
}

@Serializable
class AuthenticatorAttestationResponse(
    override val clientDataJSON: String,
    val attestationObject: String,
    val transports: List<@AuthenticatorTransport String>
) : AuthenticatorResponse

@Serializable
data class AuthenticatorAssertionResponse(
    override val clientDataJSON: String,
    val authenticatorData: String,
    val signature: String,
    val userHandle: String?
) : AuthenticatorResponse

object AuthenticatorResponseSerializer :
    JsonContentPolymorphicSerializer<AuthenticatorResponse>(AuthenticatorResponse::class) {
    override fun selectDeserializer(element: JsonElement) = when {
        "attestationObject" in element.jsonObject -> AuthenticatorAttestationResponse.serializer()
        else -> AuthenticatorAssertionResponse.serializer()
    }
}
