package dev.dc.passkeys.demo.common.model.resource

import io.ktor.resources.*

@Resource("/api")
class Api private constructor() {
    @Resource("credentials")
    class Credentials(val parent: Api = Api(), val name: String? = "foobar") {
        @Resource("{id}")
        class ById(val parent: Credentials = Credentials(), val id: String)
    }

    @Resource("credential-creation-options")
    class CredentialCreationOptions(val parent: Api = Api())

    @Resource("credential-request-options")
    class CredentialRequestOptions(val parent: Api = Api())
}
