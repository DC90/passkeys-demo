package dev.dc.passkeys.demo.common.model.entity

@Target(AnnotationTarget.PROPERTY, AnnotationTarget.TYPE)
annotation class CredentialMediationRequirement {
    companion object {
        @CredentialMediationRequirement
        const val CONDITIONAL = "conditional"

        @CredentialMediationRequirement
        const val OPTIONAL = "optional"

        @CredentialMediationRequirement
        const val REQUIRED = "required"

        @CredentialMediationRequirement
        const val SILENT = "silent"
    }
}
