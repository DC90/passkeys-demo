package dev.dc.passkeys.demo.common.model.entity

import kotlinx.serialization.Contextual
import kotlinx.serialization.Serializable

@Serializable
data class PublicKeyCredentialRequestOptions(
    val challenge: String,
    val timeout: Int,
    val rpId: String,
    val userVerification: @UserVerificationRequirement String = UserVerificationRequirement.PREFERRED,
    val allowCredentials: List<PublicKeyCredentialDescriptor> = emptyList(),
    val extensions: Map<@Contextual Any, @Contextual Any>? = null
) {
    companion object {
        const val PATH = "/credential-request-options"
    }
}
