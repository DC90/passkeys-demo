package dev.dc.passkeys.demo.common.model.dto

import dev.dc.passkeys.demo.common.model.entity.CredentialCreationOptions
import kotlinx.serialization.Serializable

@Serializable
data class PostCredentialCreationOptionsRequest(
    val displayName: String,
    val username: String
)

typealias PostCredentialCreationOptionsResponse = CredentialCreationOptions
