package dev.dc.passkeys.demo.common.model.entity

import kotlinx.serialization.Serializable

@Serializable
data class PublicKeyCredentialParameters(
    val type: @PublicKeyCredentialType String,
    val alg: @COSEAlgorithmIdentifier Int
)
