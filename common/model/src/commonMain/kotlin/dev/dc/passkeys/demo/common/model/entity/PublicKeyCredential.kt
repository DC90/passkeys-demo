package dev.dc.passkeys.demo.common.model.entity

import kotlinx.serialization.Serializable

@Serializable
data class PublicKeyCredential(
    val id: String,
    val rawId: String,
    val response: AuthenticatorResponse,
    val type: @PublicKeyCredentialType String,
    val authenticatorAttachment: @AuthenticatorAttachment String? = null
)
