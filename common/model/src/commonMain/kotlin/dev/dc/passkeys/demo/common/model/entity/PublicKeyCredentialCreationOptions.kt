package dev.dc.passkeys.demo.common.model.entity

import kotlinx.serialization.Contextual
import kotlinx.serialization.Serializable

@Serializable
data class PublicKeyCredentialCreationOptions(
    val rp: PublicKeyCredentialRpEntity,
    val user: PublicKeyCredentialUserEntity,
    val challenge: String,
    val pubKeyCredParams: Collection<PublicKeyCredentialParameters>,
    val authenticatorSelection: AuthenticatorSelectionCriteria? = null,
    val excludeCredentials: Collection<PublicKeyCredentialDescriptor> = emptyList(),
    val attestation: @AttestationConveyancePreference String = AttestationConveyancePreference.NONE,
    val extensions: Map<@Contextual Any, @Contextual Any>? = null,
    val timeout: Int? = null
)
