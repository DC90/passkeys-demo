package dev.dc.passkeys.demo.common.model.entity

@Target(AnnotationTarget.PROPERTY, AnnotationTarget.TYPE)
@Retention(AnnotationRetention.SOURCE)
annotation class AuthenticatorTransport {
    companion object {
        @AuthenticatorTransport
        const val USB = "usb"

        @AuthenticatorTransport
        const val NFC = "nfc"

        @AuthenticatorTransport
        const val BLE = "ble"

        @AuthenticatorTransport
        const val SMART_CARD = "smart-card"

        @AuthenticatorTransport
        const val HYBRID = "hybrid"

        @AuthenticatorTransport
        const val INTERNAL = "internal"
    }
}
