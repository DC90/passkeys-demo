package dev.dc.passkeys.demo.common.model.entity

import kotlinx.serialization.Serializable

@Serializable
data class PublicKeyCredentialDescriptor(
    val id: String,
    val type: @PublicKeyCredentialType String,
    val transports: Collection<@AuthenticatorTransport String> = emptyList()
)
