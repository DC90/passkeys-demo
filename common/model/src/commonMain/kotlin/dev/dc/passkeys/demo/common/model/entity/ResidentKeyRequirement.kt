package dev.dc.passkeys.demo.common.model.entity

@Target(AnnotationTarget.PROPERTY, AnnotationTarget.TYPE)
@Retention(AnnotationRetention.SOURCE)
annotation class ResidentKeyRequirement {
    companion object {
        /**
         * The Relying Party prefers creating a server-side credential, but will accept a
         * client-side discoverable credential. The client and authenticator SHOULD create
         * a server-side credential if possible.
         */
        @ResidentKeyRequirement
        const val DISCOURAGED = "discouraged"

        /**
         * The Relying Party strongly prefers creating a client-side discoverable credential, but
         * will accept a server-side credential. The client and authenticator SHOULD create a
         * discoverable credential if possible. For example, the client SHOULD guide the user
         * through setting up user verification if needed to create a discoverable credential.
         * This takes precedence over the setting of userVerification.
         */
        @ResidentKeyRequirement
        const val PREFERRED = "preferred"

        /**
         * The Relying Party requires a client-side discoverable credential. The client MUST
         * return an error if a client-side discoverable credential cannot be created.
         */
        @ResidentKeyRequirement
        const val REQUIRED = "required"
    }
}
