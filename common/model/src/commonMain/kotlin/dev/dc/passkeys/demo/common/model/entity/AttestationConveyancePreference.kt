package dev.dc.passkeys.demo.common.model.entity

@Target(AnnotationTarget.PROPERTY, AnnotationTarget.TYPE)
@Retention(AnnotationRetention.SOURCE)
annotation class AttestationConveyancePreference {
    companion object {
        @AttestationConveyancePreference
        const val NONE = "none"

        @AttestationConveyancePreference
        const val INDIRECT = "indirect"

        @AttestationConveyancePreference
        const val DIRECT = "direct"

        @AttestationConveyancePreference
        const val ENTERPRISE = "enterprise"
    }
}
