package dev.dc.passkeys.demo.common.model.entity

import kotlinx.serialization.Serializable

@Serializable
data class PublicKeyCredentialUserEntity(
    val id: String,
    val name: String,
    val displayName: String? = null
)
