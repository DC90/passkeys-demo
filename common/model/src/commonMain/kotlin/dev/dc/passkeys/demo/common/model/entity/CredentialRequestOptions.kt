package dev.dc.passkeys.demo.common.model.entity

import io.ktor.resources.*
import kotlinx.serialization.Serializable

@Resource("/credential-request-options")
data class CredentialRequestOptions(
    val publicKey: PublicKeyCredentialRequestOptions,
    val mediation: @CredentialMediationRequirement String
) {
    @Serializable
    data class Post(
        val username: String
    )
}
