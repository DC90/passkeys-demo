package dev.dc.passkeys.demo.common.model.entity

@Target(AnnotationTarget.TYPE, AnnotationTarget.PROPERTY)
@Retention(AnnotationRetention.SOURCE)
annotation class AuthenticatorAttachment {
    companion object {
        /**
         * A platform authenticator is attached using a client device-specific transport, called platform
         * attachment, and is usually not removable from the client device. A public key credential bound
         * to a platform authenticator is called a platform credential.
         */
        @AuthenticatorAttachment
        const val PLATFORM = "platform"

        /**
         * A roaming authenticator is attached using cross-platform transports, called cross-platform attachment.
         * Authenticators of this class are removable from, and can "roam" between, client devices. A public key
         * credential bound to a roaming authenticator is called a roaming credential.
         */
        @AuthenticatorAttachment
        const val CROSS_PLATFORM = "cross-platform"
    }
}
