package dev.dc.passkeys.demo.common.model.dto

import dev.dc.passkeys.demo.common.model.entity.CredentialRequestOptions
import kotlinx.serialization.Serializable

@Serializable
data class PostCredentialRequestOptionsRequest(
    val username: String
)

typealias PostCredentialRequestOptionsResponse = CredentialRequestOptions
