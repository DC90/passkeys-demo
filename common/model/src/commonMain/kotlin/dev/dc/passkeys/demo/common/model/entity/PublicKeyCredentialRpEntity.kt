package dev.dc.passkeys.demo.common.model.entity

import kotlinx.serialization.Serializable

@Serializable
data class PublicKeyCredentialRpEntity(
    val id: String,
    val name: String
)
