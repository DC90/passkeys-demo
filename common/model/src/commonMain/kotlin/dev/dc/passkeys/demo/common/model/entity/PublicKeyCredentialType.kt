package dev.dc.passkeys.demo.common.model.entity

@Target(AnnotationTarget.PROPERTY, AnnotationTarget.TYPE)
@Retention(AnnotationRetention.SOURCE)
annotation class PublicKeyCredentialType {
    companion object {
        @PublicKeyCredentialType
        const val PUBLIC_KEY = "public-key"
    }
}
