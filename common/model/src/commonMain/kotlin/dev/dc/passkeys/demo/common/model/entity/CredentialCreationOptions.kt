package dev.dc.passkeys.demo.common.model.entity

import kotlinx.serialization.Serializable

@Serializable
data class CredentialCreationOptions(
    val publicKey: PublicKeyCredentialCreationOptions
)
