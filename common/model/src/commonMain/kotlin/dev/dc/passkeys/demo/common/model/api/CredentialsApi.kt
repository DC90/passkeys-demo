package dev.dc.passkeys.demo.common.model.api

import dev.dc.passkeys.demo.common.model.dto.PostCredentialCreationOptionsRequest
import dev.dc.passkeys.demo.common.model.dto.PostCredentialCreationOptionsResponse
import dev.dc.passkeys.demo.common.model.dto.PostCredentialRequestOptionsRequest
import dev.dc.passkeys.demo.common.model.dto.PostCredentialRequestOptionsResponse
import dev.dc.passkeys.demo.common.model.entity.PublicKeyCredential

interface CredentialsApi {
    suspend fun getCredentials(): Collection<PublicKeyCredential>
    suspend fun getCredential(id: String): PublicKeyCredential
    suspend fun postCredential(credential: PublicKeyCredential): PublicKeyCredential
    suspend fun postCredentialCreationOptions(request: PostCredentialCreationOptionsRequest): PostCredentialCreationOptionsResponse
    suspend fun postCredentialRequestOptions(request: PostCredentialRequestOptionsRequest): PostCredentialRequestOptionsResponse
}
