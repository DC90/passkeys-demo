plugins {
    id("com.android.application")
    id("com.google.dagger.hilt.android")
    id("com.google.gms.google-services")
    id("org.jetbrains.compose")
    kotlin("android")
    kotlin("kapt")
}

group = "${rootProject.group}.android"
version = rootProject.version

android {
    namespace = group.toString()
    compileSdk = 33
    compileSdkPreview = "UpsideDownCake"

    defaultConfig {
        minSdk = 29
        targetSdk = 33
        versionCode = 1
        versionName = version.toString()
    }

    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_17
        targetCompatibility = JavaVersion.VERSION_17
    }

    kotlinOptions {
        jvmTarget = "17"
    }

    buildFeatures {
        compose = true
    }

    buildTypes {
        debug {
            signingConfig = signingConfigs["debug"]
        }
    }
}

dependencies {
    implementation(project(":common:model"))
    implementation(project(":common:client"))

    implementation("androidx.activity:activity-compose:1.7.2")
    implementation(libs.bundles.androidx.credentials)
    implementation("com.google.dagger:hilt-android:2.44")

    implementation(compose.material3)
    implementation(compose.preview)
    implementation(compose.runtime)
    implementation(compose.ui)
    implementation(compose.uiTooling)

    implementation(libs.kotlinx.serialization.json)
    implementation(libs.ktor.client.okhttp)

    implementation(platform("com.google.firebase:firebase-bom:32.1.0"))
    implementation("com.google.firebase:firebase-auth-ktx")
    implementation("com.firebaseui:firebase-ui-auth:7.2.0")

    kapt("com.google.dagger:hilt-android-compiler:2.44")
}
