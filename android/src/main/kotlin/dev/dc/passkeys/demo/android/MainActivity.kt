@file:OptIn(ExperimentalMaterial3Api::class)

package dev.dc.passkeys.demo.android

import android.app.Activity
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Scaffold
import androidx.compose.runtime.*
import androidx.compose.ui.Modifier
import androidx.lifecycle.lifecycleScope
import com.firebase.ui.auth.AuthUI
import com.firebase.ui.auth.AuthUI.IdpConfig
import com.firebase.ui.auth.FirebaseAuthUIActivityResultContract
import dagger.hilt.android.AndroidEntryPoint
import dev.dc.passkeys.demo.android.ui.component.AppBar
import dev.dc.passkeys.demo.android.ui.component.AppContent
import dev.dc.passkeys.demo.android.ui.theme.PasskeysDemoTheme
import dev.dc.passkeys.demo.common.client.usecase.PasskeysUseCase
import kotlinx.coroutines.launch
import javax.inject.Inject

@AndroidEntryPoint
class MainActivity : ComponentActivity() {
    companion object {
        private const val TAG = "MainActivity"
        private val Google: IdpConfig = IdpConfig.GoogleBuilder().build()
        private val GitHub: IdpConfig = IdpConfig.GitHubBuilder().build()
    }

    @Inject
    lateinit var passkeysUseCase: PasskeysUseCase

    // TODO
    private val signInLauncher = registerForActivityResult(FirebaseAuthUIActivityResultContract()) {
        when (it.resultCode) {
            Activity.RESULT_OK -> {
            }
        }
    }

    private fun signUpIntent() = AuthUI.getInstance()
        .createSignInIntentBuilder()
        .setAvailableProviders(listOf(Google, GitHub))
        .build()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            var name by remember { mutableStateOf("") }
            var email by remember { mutableStateOf("") }

            val clearInputs = {
                name = ""
                email = ""
            }

            App(
                name = name,
                onNameChange = { name = it },
                email = email,
                onEmailChange = { email = it },
                onCreatePasskeyClick = {
                    onCreatePasskeyClick(name, email, clearInputs)
                },
                onSignInWithPasskeyClick = {
                    onSignInWithPasskeyClick(clearInputs)
                }
            )
        }
    }

    private fun onCreatePasskeyClick(name: String, email: String, onComplete: () -> Unit) {
        lifecycleScope.launch {
            passkeysUseCase.createPasskey(name, email)
                .onSuccess {
                    Toast.makeText(this@MainActivity, "Created passkey!", Toast.LENGTH_LONG).show()
                }
                .onFailure {
                    Log.e(TAG, "Failed to create passkey", it)
                    Toast.makeText(this@MainActivity, "Failed to create passkey", Toast.LENGTH_LONG).show()
                }
                .also { onComplete() }
        }
    }

    private fun onSignInWithPasskeyClick(onComplete: () -> Unit) {
        lifecycleScope.launch {
            passkeysUseCase.getPasskey()
                .onSuccess {
                    Toast.makeText(this@MainActivity, "Signed in with passkey!", Toast.LENGTH_LONG).show()
                }
                .onFailure {
                    Log.e(TAG, "Failed to sign in with passkey", it)
                    Toast.makeText(this@MainActivity, "Failed to sign in", Toast.LENGTH_LONG).show()
                }
                .also { onComplete() }
        }
    }
}

@Composable
fun App(
    name: String,
    onNameChange: (String) -> Unit,
    email: String,
    onEmailChange: (String) -> Unit,
    onCreatePasskeyClick: () -> Unit,
    onSignInWithPasskeyClick: () -> Unit,
    modifier: Modifier = Modifier
) {
    PasskeysDemoTheme {
        Scaffold(
            modifier = modifier,
            topBar = {
                AppBar()
            }
        ) {
            AppContent(
                name = name,
                onNameChange = onNameChange,
                email = email,
                onEmailChange = onEmailChange,
                onCreatePasskeyClick = onCreatePasskeyClick,
                onSignInWithPasskeyClick = onSignInWithPasskeyClick,
                modifier = Modifier.padding(it)
            )
        }
    }
}
