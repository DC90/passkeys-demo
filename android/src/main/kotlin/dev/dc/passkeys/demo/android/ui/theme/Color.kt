package dev.dc.passkeys.demo.android.ui.theme

import androidx.compose.ui.graphics.Color

val Blue200 = Color(0xFFD2E3FC)
val Blue500 = Color(0xFF538FF7)
val Blue700 = Color(0xFF185ABC)
val Yellow200 = Color(0xFFFBBC04)
val Yellow700 = Color(0xFFEA8600)
val TextColor = Color(0xFF3A3A3A)
