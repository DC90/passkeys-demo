package dev.dc.passkeys.demo.android

import android.content.Context
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ActivityComponent
import dagger.hilt.android.qualifiers.ActivityContext
import dev.dc.passkeys.demo.common.client.api.CredentialsClientApi
import dev.dc.passkeys.demo.common.client.usecase.AndroidCredentialsInteractor
import dev.dc.passkeys.demo.common.client.usecase.PasskeysUseCase

@Module
@InstallIn(ActivityComponent::class)
object HiltModule {
    @Provides
    fun providePasskeysUseCase(@ActivityContext context: Context): PasskeysUseCase = PasskeysUseCase(
        api = CredentialsClientApi("https://passkeysdemo-a922a.ew.r.appspot.com"),
        interactor = AndroidCredentialsInteractor(
            context = context
        )
    )
}
