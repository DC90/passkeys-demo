package dev.dc.passkeys.demo.android.ui.component

import androidx.compose.foundation.layout.*
import androidx.compose.material3.*
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import dev.dc.passkeys.demo.android.ui.theme.TextColor

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun CreatePasskeyForm(
    name: String,
    onNameChange: (String) -> Unit,
    email: String,
    onEmailChange: (String) -> Unit,
    onSubmit: () -> Unit
) {
    Column(
        verticalArrangement = Arrangement.spacedBy(8.dp),
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        Text(
            text = "Provide your details to create a passkey",
            style = MaterialTheme.typography.bodyMedium,
            color = MaterialTheme.colorScheme.onBackground
        )

        OutlinedTextField(
            value = name,
            onValueChange = onNameChange,
            modifier = Modifier.fillMaxWidth(),
            textStyle = MaterialTheme.typography.bodyMedium,
            label = {
                Text(
                    text = "Full name",
                    style = MaterialTheme.typography.bodyMedium
                )
            },
            singleLine = true
        )

        OutlinedTextField(
            value = email,
            onValueChange = onEmailChange,
            modifier = Modifier.fillMaxWidth(),
            textStyle = MaterialTheme.typography.bodyMedium,
            label = {
                Text(
                    text = "Email address",
                    style = MaterialTheme.typography.bodyMedium
                )
            },
            singleLine = true
        )

        Spacer(modifier = Modifier.height(8.dp))

        Button(
            onClick = onSubmit,
            modifier = Modifier
                .fillMaxWidth()
                .height(48.dp),
            shape = ShapeDefaults.Small,
            elevation = ButtonDefaults.buttonElevation(2.dp)
        ) {
            Text(
                text = "Create a Passkey",
                fontWeight = FontWeight.Medium
            )
        }
    }
}
