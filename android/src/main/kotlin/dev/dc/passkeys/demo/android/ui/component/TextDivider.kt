package dev.dc.passkeys.demo.android.ui.component

import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.Divider
import androidx.compose.material3.DividerDefaults
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp

@Composable
fun TextDivider(
    text: String,
    modifier: Modifier = Modifier,
    color: Color = DividerDefaults.color
) {
    Row(
        verticalAlignment = Alignment.CenterVertically,
        modifier = modifier.fillMaxWidth()
    ) {
        Divider(
            thickness = 1.dp,
            modifier = Modifier.weight(1.0f),
            color = color
        )

        Text(
            text = text,
            fontSize = 12.sp,
            color = color,
            modifier = Modifier.padding(horizontal = 8.dp)
        )

        Divider(
            thickness = 1.dp,
            modifier = Modifier.weight(1.0f),
            color = color
        )
    }
}
