plugins {
    application
    kotlin("jvm")
    id("com.github.johnrengelman.shadow")
    id("com.google.cloud.tools.appengine") version "2.4.5"
}

group = "${rootProject.group}.server"
version = rootProject.version

application {
    mainClass.set("io.ktor.server.netty.EngineMain")
}

kotlin {
    jvmToolchain(17)
}

dependencies {
    implementation(project(":common:model"))
    implementation(libs.kotlinx.serialization.json)
    implementation(libs.ktor.serialization.kotlinx.json)
    implementation(libs.ktor.server.auth)
    implementation(libs.ktor.server.compression)
    implementation(libs.ktor.server.contentnegotiation)
    implementation(libs.ktor.server.core)
    implementation(libs.ktor.server.cors)
    implementation(libs.ktor.server.config.yaml)
    implementation(libs.ktor.server.netty)
    implementation(libs.ktor.server.resources)
    implementation(libs.logback)

    implementation("com.google.firebase:firebase-admin:9.0.0")
    implementation("org.jetbrains.kotlinx:kotlinx-serialization-cbor:1.5.1")
}

appengine {
    stage {
        setArtifact("build/libs/${project.name}-${project.version}-all.jar")
    }
    deploy {
        version = "GCLOUD_CONFIG"
        projectId = "GCLOUD_CONFIG"
    }
}
