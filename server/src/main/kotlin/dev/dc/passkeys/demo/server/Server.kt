package dev.dc.passkeys.demo.server

import dev.dc.passkeys.demo.server.route.credentialCreationOptions
import dev.dc.passkeys.demo.server.route.credentialRequestOptions
import dev.dc.passkeys.demo.server.route.credentials
import io.ktor.http.*
import io.ktor.serialization.kotlinx.json.*
import io.ktor.server.application.*
import io.ktor.server.netty.*
import io.ktor.server.plugins.compression.*
import io.ktor.server.plugins.contentnegotiation.*
import io.ktor.server.plugins.cors.routing.*
import io.ktor.server.resources.*
import io.ktor.server.routing.*

fun main(args: Array<String>) {
    EngineMain.main(args)
}

@Suppress("unused")
fun Application.module() {
    install(Compression) {
        gzip()
    }
    install(ContentNegotiation) {
        json()
    }
    install(CORS) {
        allowMethod(HttpMethod.Get)
        allowMethod(HttpMethod.Post)
        allowMethod(HttpMethod.Options)
        allowHeader(HttpHeaders.Accept)
        allowHeader(HttpHeaders.ContentType)
        anyHost()
    }
    install(Resources)

    routing {
        credentialCreationOptions()
        credentialRequestOptions()
        credentials()
    }
}
