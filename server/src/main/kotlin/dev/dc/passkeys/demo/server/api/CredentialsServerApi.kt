package dev.dc.passkeys.demo.server.api

import dev.dc.passkeys.demo.common.model.api.CredentialsApi
import dev.dc.passkeys.demo.common.model.dto.PostCredentialCreationOptionsRequest
import dev.dc.passkeys.demo.common.model.dto.PostCredentialRequestOptionsRequest
import dev.dc.passkeys.demo.common.model.entity.*
import java.security.MessageDigest
import java.security.SecureRandom
import kotlin.io.encoding.Base64
import kotlin.io.encoding.ExperimentalEncodingApi

@OptIn(ExperimentalEncodingApi::class)
class CredentialsServerApi : CredentialsApi {
    companion object {
        private const val timeout = 418000
        private val pubKeyCredParams = listOf(
            PublicKeyCredentialParameters(
                type = PublicKeyCredentialType.PUBLIC_KEY,
                alg = COSEAlgorithmIdentifier.ES256
            ),
            PublicKeyCredentialParameters(
                type = PublicKeyCredentialType.PUBLIC_KEY,
                alg = COSEAlgorithmIdentifier.RS256
            )
        )
        private val rp = PublicKeyCredentialRpEntity(
            id = "passkeysdemo-a922a.web.app",
            name = "Passkeys Demo"
        )
    }

    override suspend fun getCredentials(): Collection<PublicKeyCredential> {
        TODO("Not yet implemented")
    }

    override suspend fun getCredential(id: String): PublicKeyCredential {
        TODO("Not yet implemented")
    }

    override suspend fun postCredential(credential: PublicKeyCredential): PublicKeyCredential {
        when (val response = credential.response) {
            is AuthenticatorAttestationResponse -> {
                println(response)
            }

            is AuthenticatorAssertionResponse -> {
                println(response)
            }
        }
        return credential
    }

    override suspend fun postCredentialCreationOptions(request: PostCredentialCreationOptionsRequest): CredentialCreationOptions {
        val userId = generateUserId(request.username)
        val challenge = generateChallenge()

        return CredentialCreationOptions(
            publicKey = PublicKeyCredentialCreationOptions(
                rp = rp,
                challenge = challenge,
                user = PublicKeyCredentialUserEntity(
                    id = userId,
                    name = request.username,
                    displayName = request.displayName
                ),
                pubKeyCredParams = pubKeyCredParams,
                authenticatorSelection = AuthenticatorSelectionCriteria(
                    residentKey = ResidentKeyRequirement.REQUIRED,
                    userVerification = UserVerificationRequirement.REQUIRED
                ),
                timeout = timeout
            )
        )
    }

    override suspend fun postCredentialRequestOptions(request: PostCredentialRequestOptionsRequest): CredentialRequestOptions {
        val challenge = generateChallenge()

        return CredentialRequestOptions(
            mediation = CredentialMediationRequirement.OPTIONAL,
            publicKey = PublicKeyCredentialRequestOptions(
                challenge = challenge,
                timeout = timeout,
                rpId = rp.id
            )
        )
    }

    private fun generateUserId(username: String) = Base64.UrlSafe.encode(
        sha256(username.encodeToByteArray())
    )

    private fun generateChallenge() = Base64.UrlSafe.encode(
        ByteArray(64).apply {
            SecureRandom().nextBytes(this)
        }
    )
}

private fun sha256(bytes: ByteArray) = MessageDigest.getInstance("SHA256").digest(bytes)
