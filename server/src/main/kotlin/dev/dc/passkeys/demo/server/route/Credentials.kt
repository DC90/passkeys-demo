package dev.dc.passkeys.demo.server.route

import dev.dc.passkeys.demo.common.model.api.CredentialsApi
import dev.dc.passkeys.demo.common.model.entity.PublicKeyCredential
import dev.dc.passkeys.demo.common.model.resource.Api
import dev.dc.passkeys.demo.server.api.CredentialsServerApi
import io.ktor.server.application.*
import io.ktor.server.request.*
import io.ktor.server.resources.*
import io.ktor.server.response.*
import io.ktor.server.routing.*
import io.ktor.server.routing.post

fun Routing.credentials() {
    val api: CredentialsApi = CredentialsServerApi()

    get<Api.Credentials> {
        api.getCredentials()
            .let { call.respond(it) }
    }

    get<Api.Credentials.ById> { credential ->
        api.getCredential(credential.id)
            .let { call.respond(it) }
    }

    post<Api.Credentials> {
        call.receive<PublicKeyCredential>()
            .let { api.postCredential(it) }
            .let { call.respond(it) }
    }
}
