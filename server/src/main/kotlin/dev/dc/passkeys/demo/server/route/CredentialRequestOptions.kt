package dev.dc.passkeys.demo.server.route

import dev.dc.passkeys.demo.common.model.api.CredentialsApi
import dev.dc.passkeys.demo.common.model.dto.PostCredentialRequestOptionsRequest
import dev.dc.passkeys.demo.common.model.entity.PublicKeyCredentialRequestOptions
import dev.dc.passkeys.demo.common.model.resource.Api
import dev.dc.passkeys.demo.server.api.CredentialsServerApi
import io.ktor.server.application.*
import io.ktor.server.request.*
import io.ktor.server.response.*
import io.ktor.server.routing.*

fun Routing.credentialRequestOptions() = route(PublicKeyCredentialRequestOptions.PATH) {
    val api: CredentialsApi = CredentialsServerApi()

    post<Api.CredentialRequestOptions> {
        call.receive<PostCredentialRequestOptionsRequest>()
            .let { api.postCredentialRequestOptions(it) }
            .let { call.respond(it) }
    }
}
