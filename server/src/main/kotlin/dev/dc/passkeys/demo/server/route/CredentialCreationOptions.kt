package dev.dc.passkeys.demo.server.route

import dev.dc.passkeys.demo.common.model.api.CredentialsApi
import dev.dc.passkeys.demo.common.model.dto.PostCredentialCreationOptionsRequest
import dev.dc.passkeys.demo.common.model.resource.Api
import dev.dc.passkeys.demo.server.api.CredentialsServerApi
import io.ktor.server.application.*
import io.ktor.server.request.*
import io.ktor.server.response.*
import io.ktor.server.routing.*

fun Routing.credentialCreationOptions() {
    val api: CredentialsApi = CredentialsServerApi()

    post<Api.CredentialCreationOptions> {
        call.receive<PostCredentialCreationOptionsRequest>()
            .let { api.postCredentialCreationOptions(it) }
            .let { call.respond(it) }
    }
}
