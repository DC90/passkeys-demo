import dev.dc.passkeys.demo.common.client.api.CredentialsClientApi
import dev.dc.passkeys.demo.common.client.usecase.JsCredentialsInteractor
import dev.dc.passkeys.demo.common.client.usecase.PasskeysUseCase
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.promise

class CredentialManager(
    private val useCase: PasskeysUseCase = PasskeysUseCase(
        CredentialsClientApi("https://passkeysdemo-a922a.ew.r.appspot.com"),
        JsCredentialsInteractor()
    )
) {
    private val coroutineScope = CoroutineScope(Dispatchers.Unconfined)

    fun onSignUp(name: String, email: String) = coroutineScope.promise {
        useCase.createPasskey(name, email)
            .onSuccess {
                console.log("Created passkey: ${it.id}")
            }
            .onFailure {
                console.log(it)
            }
    }

    fun onSignIn() = coroutineScope.promise {
        useCase.getPasskey()
            .onSuccess {
                console.log("Authenticated with passkey: ${it.id}")
            }
            .onFailure {
                console.log(it)
            }
    }
}
