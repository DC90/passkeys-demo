package components

import emotion.react.css
import react.FC
import react.Props
import react.dom.html.ReactHTML
import web.cssom.*

external interface AlertProps : Props {
    var type: String
    var message: String
}

val Alert = FC<AlertProps> {
    ReactHTML.div {
        css {
            borderRadius = 8.px
            background = when (it.type) {
                "SUCCESS" -> Color("#65B867")
                "FAILURE" -> Color("#EC5E4F")
                else -> Color("#F6BD41")
            }
            color = NamedColor.white
            display = Display.flex
            flexDirection = FlexDirection.column
            margin = Margin(vertical = 20.px, horizontal = 0.px)
            padding = 16.px
            width = 400.px
        }

        +it.message
    }
}
