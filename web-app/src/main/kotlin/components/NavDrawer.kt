package components

import mui.icons.material.Source
import mui.icons.material.WidthWide
import mui.material.*
import mui.system.sx
import react.FC
import react.Props
import web.cssom.*

external interface NavDrawerProps : Props {
    var open: Boolean
    var setOpen: (Boolean) -> Unit
}

val NavDrawer = FC<NavDrawerProps> {
    Drawer {
        anchor = DrawerAnchor.left
        open = it.open
        variant = DrawerVariant.persistent
        sx {
            display = Display.flex
            flexShrink = number(0.0)

            width = 256.px
            and(MuiDrawer.paper) {
                width = 256.px
                boxSizing = BoxSizing.borderBox
            }
        }

        NavDrawerHeader {
            onButtonClick = { it.setOpen(false) }
        }

        Divider {}

        List {
            listOf("Projects", "Groups", "Issues", "Milestones").forEach {
                ListItem {
                    key = it
                    disablePadding = true

                    ListItemButton {
                        ListItemIcon {
                            Source {}
                        }
                        ListItemText {
                            sx {
                                flexGrow = number(1.0)
                                flexShrink = number(0.0)
                            }
                            +it
                        }
                    }
                }
            }
        }
    }
}
