package components

import mui.icons.material.Source
import mui.material.*
import mui.material.styles.Theme
import mui.material.styles.TypographyVariant
import mui.material.styles.useTheme
import mui.system.sx
import react.VFC
import react.dom.html.ReactHTML.div
import react.dom.html.ReactHTML.main
import web.cssom.*

const val drawerWidth = 256

val ClippedDrawer = VFC {
    val theme = useTheme<Theme>()

    Box {
        sx {
            display = Display.flex
        }

        CssBaseline()

        AppBar {
            sx {
                zIndex = integer(theme.zIndex.drawer.toInt() + 1)
            }

            position = AppBarPosition.fixed

            Toolbar {
                Typography {
                    component = div
                    noWrap = true
                    variant = TypographyVariant.h6

                    +"Title"
                }
            }
        }

        Drawer {
            variant = DrawerVariant.permanent

            sx {
                minWidth = drawerWidth.px
            }

            Box {
                sx {
                    overflow = Auto.auto
                    width = drawerWidth.px
                }

                Toolbar()

                List {
                    listOf("Projects", "Groups", "Issues", "Milestones").map {
                        ListItem {
                            key = it
                            disablePadding = true

                            ListItemButton {
                                ListItemIcon {
                                    Source()
                                }

                                ListItemText {
                                    +it
                                }
                            }
                        }
                    }
                }
            }
        }

        Box {
            sx {
                flexGrow = number(1.0)
                padding = 2.px
            }

            component = main

            Toolbar()

            Typography {
                paragraph = true

                +"Lorem ipsum dolor sit amet"
            }
        }
    }
}
