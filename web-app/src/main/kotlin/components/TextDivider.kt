package components

import emotion.react.css
import react.FC
import react.Props
import react.dom.html.ReactHTML.div
import react.dom.html.ReactHTML.span
import web.cssom.*

external interface DividerProps : Props {
    var text: String?
}

val TextDivider = FC<DividerProps> {
    div {
        span {
            css {
                color = NamedColor.darkgray
                display = Display.flex
                flexDirection = FlexDirection.row
                alignItems = AlignItems.center
                fontSize = 12.px

                before {
                    content = Content()
                    display = Display.inlineBlock
                    width = 100.pct
                    height = 1.px
                    marginInline = 16.px
                    background = NamedColor.lightgray
                }

                after {
                    content = Content()
                    display = Display.inlineBlock
                    height = 1.px
                    width = 100.pct
                    marginInline = 16.px
                    background = NamedColor.lightgray
                }
            }

            +it.text
        }
    }
}
