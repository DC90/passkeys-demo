package components

import emotion.react.css
import react.FC
import react.Props
import react.dom.html.ReactHTML.button
import react.dom.html.ReactHTML.div
import styles.StyleSheet
import web.cssom.*

external interface SignInWithPasskeyProps : Props {
    var onClick: () -> Unit
}

val SignInWithPasskey = FC<SignInWithPasskeyProps> { props ->
    div {
        css {
            display = Display.flex
            flexDirection = FlexDirection.column
        }

        button {
            css(StyleSheet.secondaryButton) {}

            onClick = {
                props.onClick()
            }

            +"Sign in with Passkey"
        }
    }
}
