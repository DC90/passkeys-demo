package components

import emotion.react.css
import mui.icons.material.ChevronLeftOutlined
import mui.material.IconButton
import mui.material.styles.Theme
import mui.material.styles.useTheme
import react.FC
import react.Props
import react.dom.html.ReactHTML.div
import web.cssom.AlignItems
import web.cssom.Display
import web.cssom.JustifyContent
import web.cssom.px

external interface NavDrawerHeaderProps : Props {
    var onButtonClick: () -> Unit
}

val NavDrawerHeader = FC<NavDrawerHeaderProps> { props ->
    val theme = useTheme<Theme>()

    div {
        css {
            alignItems = AlignItems.center
            display = Display.flex
            justifyContent = JustifyContent.flexEnd
            height = 64.px
            width = 256.px
            padding = theme.spacing(0, 1)
        }

        IconButton {
            onClick = { props.onButtonClick() }

            ChevronLeftOutlined {}
        }
    }
}
