import components.AlertProps
import components.ClippedDrawer
import components.NavDrawerHeader
import emotion.react.css
import js.core.jso
import mui.icons.material.Menu
import mui.icons.material.Source
import mui.material.*
import mui.material.styles.Theme
import mui.material.styles.createTheme
import mui.material.styles.useTheme
import mui.system.sx
import react.StateSetter
import react.VFC
import react.create
import react.dom.client.createRoot
import react.dom.html.ReactHTML.div
import react.useState
import web.cssom.*
import web.dom.document

val credentialManager = CredentialManager()

fun main() {
    document.createElement("div").let {
        document.body.appendChild(it)
        createRoot(it).render(App.create())
    }
}

val App = VFC {
    div {
        css {
            fontFamily = string("Google Sans,sans-serif")
        }

        val (alert, setAlert) = useState<AlertProps?>(null)

        Landing {
            onInputChange = {
                setAlert(null)
            }
            alertProps = alert
            onSubmit = { name, email ->
                onCreatePasskey(name, email, setAlert)
            }
            onSignInWithPasskey = {
                onSignInWithPasskey(setAlert)
            }
        }
    }
}

private fun onCreatePasskey(name: String, email: String, setAlert: StateSetter<AlertProps?>) {
    setAlert(null)
    credentialManager.onSignUp(name, email)
        .then {
            it.fold(
                onSuccess = {
                    setAlert(jso<AlertProps> {
                        type = "SUCCESS"
                        message = "Successfully created passkey"
                    })
                },
                onFailure = {
                    setAlert(jso<AlertProps> {
                        type = "FAILURE"
                        message = "Failed to create passkey"
                    })
                }
            )
        }
}

private fun onSignInWithPasskey(setAlert: StateSetter<AlertProps?>) {
    setAlert(null)
    credentialManager.onSignIn()
        .then {
            it.fold(
                onSuccess = {
                    setAlert(jso<AlertProps> {
                        message = "Successfully signed in"
                        type = "SUCCESS"
                    })
                },
                onFailure = {
                    setAlert(jso<AlertProps> {
                        message = "Failed to sign in"
                        type = "FAILURE"
                    })
                }
            )
        }
}
