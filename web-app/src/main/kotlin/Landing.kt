import components.Alert
import components.AlertProps
import components.SignInWithPasskey
import components.TextDivider
import csstype.PropertiesBuilder
import emotion.react.css
import react.FC
import react.Props
import react.dom.html.ReactHTML.button
import react.dom.html.ReactHTML.div
import react.dom.html.ReactHTML.h1
import react.dom.html.ReactHTML.input
import react.dom.html.ReactHTML.label
import react.useState
import styles.LightColorScheme
import styles.StyleSheet
import web.cssom.*
import web.html.InputType

external interface LandingProps : Props {
    var alertProps: AlertProps?
    var onInputChange: (String) -> Unit
    var onSubmit: (String, String) -> Unit
    var onSignInWithPasskey: () -> Unit
}

val Landing = FC<LandingProps> { props ->
    val (name, setName) = useState("")
    val (email, setEmail) = useState("")

    div {
        css {
            display = Display.flex
            flexDirection = FlexDirection.column
            justifyContent = JustifyContent.center
            textAlign = TextAlign.center
            margin = 10.pct
        }

        h1 {
            css {
                color = Color("#538FF7")
            }
            +"Passkeys Demo"
        }

        div {
            css {
                display = Display.flex
                flexDirection = FlexDirection.column
                alignItems = AlignItems.center
                justifyContent = JustifyContent.center
                alignContent = AlignContent.center
                width = 100.pct
                minWidth = 50.pct
            }

            div {
                css(StyleSheet.column, StyleSheet.card) {
                    width = 400.px
                }

                label {
                    css(formHeaderStyle)

                    +"Provide your details to create a passkey"
                }

                input {
                    css(StyleSheet.input) {}

                    type = InputType.text
                    value = name
                    placeholder = "Name"
                    onChange = {
                        props.onInputChange(it.target.value)
                        setName(it.target.value)
                    }
                }

                input {
                    css(StyleSheet.input) {}

                    type = InputType.text
                    value = email
                    placeholder = "Email"
                    onChange = {
                        props.onInputChange(it.target.value)
                        setEmail(it.target.value)
                    }
                }

                button {
                    css(StyleSheet.primaryButton) {}

                    onClick = {
                        props.onSubmit(name, email)
                    }

                    +"Create a Passkey"
                }

                TextDivider {
                    text = "or"
                }

                SignInWithPasskey {
                    onClick = props.onSignInWithPasskey
                }
            }

            props.alertProps?.let {
                Alert {
                    message = it.message
                    type = it.type
                }
            }
        }
    }
}

val formHeaderStyle: PropertiesBuilder.() -> Unit = {
    color = LightColorScheme.text
    fontSize = 20.px
    marginBlock = 16.px
    fontWeight = FontWeight.lighter
}
