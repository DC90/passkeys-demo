package styles

import emotion.css.ClassName
import web.cssom.*

interface ColorScheme {
    val background: Color
    val border: Color
    val surface: Color
    val primary: Color
    val onPrimary: Color
    val text: Color
}

object LightColorScheme : ColorScheme {
    override val background = Color("#F8F9FA")
    override val border = rgb(0, 0, 0, 0.12)
    override val surface = NamedColor.white
    override val primary = Color("#538FF7")
    override val onPrimary = surface
    override val text = Color("#121215")
}

object StyleSheet {
    private val defaultButton = ClassName {
        borderRadius = 8.px
        fontSize = FontSize.medium
        padding = 0.9.em
    }

    val primaryButton = ClassName(defaultButton) {
        backgroundColor = LightColorScheme.primary
        border = None.none
        color = LightColorScheme.onPrimary
    }

    val secondaryButton = ClassName(defaultButton) {
        backgroundColor = LightColorScheme.onPrimary
        border = Border(1.px, LineStyle.solid, LightColorScheme.primary)
        color = LightColorScheme.primary
    }

    val input = ClassName {
        border = Border(1.px, LineStyle.solid, LightColorScheme.border)
        borderRadius = 4.px
        color = LightColorScheme.text
        fontSize = FontSize.medium
        padding = 0.9.em
    }

    val column = ClassName {
        display = Display.flex
        flexDirection = FlexDirection.column
        gap = 16.px
        padding = 16.px
    }

    val card = ClassName {
        background = LightColorScheme.surface
        border = Border(1.px, LineStyle.solid, LightColorScheme.border)
        borderRadius = 8.px
    }
}
