pluginManagement {
    repositories {
        google()
        gradlePluginPortal()
        mavenCentral()
    }
    resolutionStrategy {
        eachPlugin {
            if (requested.id.id.startsWith("com.google.cloud.tools.appengine")) {
                useModule("com.google.cloud.tools:appengine-gradle-plugin:${requested.version}")
            }
        }
    }

    plugins {
        val androidVersion = "7.4.2"
        val kotlinVersion = "1.8.20"

        id("com.android.application") version androidVersion
        id("com.android.library") version androidVersion
        id("com.github.johnrengelman.shadow") version "7.1.2"
        id("com.google.dagger.hilt.android") version "2.44"
        id("com.google.gms.google-services") version "4.3.15"
        id("io.ktor.plugin") version "2.3.1"
        id("org.jetbrains.compose") version "1.4.0"
        kotlin("android") version kotlinVersion
        kotlin("js") version kotlinVersion
        kotlin("jvm") version kotlinVersion
        kotlin("multiplatform") version kotlinVersion
        kotlin("plugin.serialization") version kotlinVersion
    }
}

dependencyResolutionManagement {
    @Suppress("UnstableApiUsage")
    repositories {
        google()
        mavenCentral()
    }
}

rootProject.name = "passkeys-demo"
include(
    ":android",
    ":common:client",
    ":common:model",
    ":server",
    ":web-app"
)
